let selector = document.querySelector;
let $ = selector.bind(document);

let editor = new Behave({
    textarea: $('.main textarea'),
    replaceTab: true,
    softTabs: true,
    tabSize: 2,
    autoOpen: true,
    overwrite: true,
    autoStrip: true,
    autoIndent: true,
    fence: false
  });

$("input").onclick = function() {
    let divNode = document.createElement("div");
    let preNode = document.createElement("pre");
    let codeNode = document.createElement("code");

    divNode.style.marginTop = "10px";
    codeNode.innerText = $("textarea").value;
    codeNode.className = "language-html";
    preNode.appendChild(codeNode);
    divNode.appendChild(preNode);

    let copyNode = document.createElement("i");
    copyNode.className = "iconfont icon-clipboard";
    copyNode.title = "复制到剪贴板";

    let delNode = document.createElement("i");
    delNode.className = "iconfont icon-delete";
    delNode.title = "删除";


    divNode.appendChild(copyNode);
    divNode.appendChild(delNode);
    divNode.onclick = function(e) {
        if(e.target.className.indexOf("icon-delete") > 0) {
            e.target.parentNode.parentNode.removeChild(e.target.parentNode);
        }
        console.log("test");
        //this.parentNode.parentNode.removeChild(this.parentNode);
    }
    hljs.highlightBlock(divNode);
    $(".main").append(divNode);
}