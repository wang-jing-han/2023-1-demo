const conditional=require("koa-conditional-get")
const etag=require("koa-etag")
const Koa=require("Koa")
const app=new Koa()

app.use(conditional())
app.use(etag())

/**
 * 服务端根据当前返回的内容按照特定算法生成Etag
 * 并作为响应头返回到客户端
 * 客户端再次请求时会携带If-None-Match
 * 然后ETag和If-None-Match比对,
 * 若一致,服务端返回304，状态码，若不一致，则返回新的ETag和新的内容
 */
app.use((ctx)=>{
    ctx.body="http协商缓存之If-None-Match&&ETag11"
})

/**
 * 协商缓存 Last-modified If-Modified-Since
 * 第一次请求时，将文件的修改时间放置到Last-modified响应头中,客户端下次请求时会携带If-Modified-Since
 * 服务器收到请求，将两个值做对比，若一致则返回304
 * 若不一致，则重新返回新的Last-Modified，并返回新的文件内容
 */

app.listen(3000)
console.log("应用运行在localhost:3000")