const router = require('koa-router')()


router.get("/getdata",async(ctx,next)=>{
  /***
   * 给http响应头设置Expires字段值为格林威治时间
   * 客户端将当前时间与拿到Expries值做对比
   * 如果未超时，从本地缓存中取，如果超时，从服务器上取
   */
  let timer=new Date(Date.now()+30000)
  timer=timer.toGMTString()
  ctx.set("Expires",timer)//有效期
  ctx.body="hello http"
})

router.get("/getcache",async(ctx,next)=>{
  /**
   * 设置Cache-Control为public,max-age=30
   * 设置强制缓存，缓存的时间
   * Cache-Control优先级高于Expires
   */
  ctx.set("Cache-Control","public,max-age=30")
  ctx.body="http cache"
})

router.get('/', async (ctx, next) => {
  await ctx.render('index', {
    title: 'Hello Koa 2!'
  })
})

router.get('/string', async (ctx, next) => {
  ctx.body = 'koa2 string'
})

router.get('/json', async (ctx, next) => {
  ctx.body = {
    title: 'koa2 json'
  }
})

module.exports = router
