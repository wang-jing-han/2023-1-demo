const router = require('koa-router')()
const jwt=require("jsonwebtoken")
const mykey="214ii"

router.post("/login", async(ctx,next)=> {
  // console.log(ctx.request.body)
  let body=JSON.parse(ctx.request.body)
  let userName=body.userName
  let password=body.password
  if(userName=="zhangsan"&&password=="1234"){
    let token=jwt.sign({userName:"zhangsan"},mykey)
    ctx.body={token:token}
  }
})

router.get("/list", async(ctx,next)=> {
  await ctx.render("list",{
    title: 'Hello Koa 2!'
  })
})

router.get("/getdata", async(ctx,next)=> {
  // console.log(ctx.req.headers)
  let token=ctx.req.headers.authorization
  let result=jwt.verify(token,mykey)
  console.log(result)
})

router.get('/', async (ctx, next) => {
  await ctx.render('index', {
    title: 'Hello Koa 2!'
  })
})

router.get('/string', async (ctx, next) => {
  ctx.body = 'koa2 string'
})

router.get('/json', async (ctx, next) => {
  ctx.body = {
    title: 'koa2 json'
  }
})

module.exports = router