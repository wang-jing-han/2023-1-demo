const router = require('koa-router')()


//数据总长度
router.get("/getRouteLength", async (ctx, next) => {
    let sql = "select * from routes ORDER BY seq asc"
    let route = await ctx.db.EXCUTE(sql)
    ctx.body = route.length
})
//分页下的路线信息
router.get("/getRoute", async (ctx, next) => {
    let sql = "select * from routes ORDER BY seq asc"
    let route = await ctx.db.EXCUTE(sql)
    const page = ctx.request.url.split("=")[1]
    let newRouteList = []
    for (let i = (page - 1) * 5; i <page * 5; i++) {
        if (route[i]) { newRouteList.push(route[i]) }
    }
    ctx.body = newRouteList
})
//修改路线信息
router.post("/changeRoute", async (ctx, next) => {
    let row = ctx.request.body.row
    let sql = "update routes set routeName=?,routePhone=?,routeWay=?,routeAddress=? where routeId=?"
    let result = await ctx.db.EXCUTE(sql, [row.routeName, row.routePhone, row.routeWay, row.routeAddress, row.routeId])
    if (result.affectedRows) {
        ctx.body = { "msg": "修改成功", "code": 200 }
    }
})
//删除用户
router.post("/deleteRoute", async (ctx, next) => {
    let row = ctx.request.body.row
    let sql = "delete from routes where routeId=?"
    let result = await ctx.db.EXCUTE(sql, [row.routeId])
    if (result.affectedRows) {
        ctx.body = { "msg": "删除成功", "code": 200 }
    }
})
//添加路线
router.post("/addRoute", async (ctx, next) => {
    let row = ctx.request.body
    console.log(row.seq)
    let sql = "insert into routes (routeName,routePhone,routeWay,routeAddress,seq) values (?,?,?,?,?)"
    let result = await ctx.db.EXCUTE(sql, [row.routeName, row.routePhone, row.routeWay,row.seq, row.routeAddress])
    if (result.affectedRows) {
        ctx.body = { "msg": "添加成功", "code": 200 }
    }
})
// 置顶路线
router.post("/topRoute", async (ctx, next) => {
    let {row} = ctx.request.body

    let sql1 = "update routes set seq = seq + 1 where seq < ?";
    let result1=await ctx.db.EXCUTE(sql1, [row.seq])
    console.log(result1)

    let sql = "update routes set seq = 1 where routeId = ?";
    let result=await ctx.db.EXCUTE(sql, [row.routeId])
    console.log(result)

    if (result.affectedRows) {
        ctx.body = { "msg": "置顶成功", "code": 200 }
    }
})
//搜索路线
router.post("/searchRoute", async (ctx, next) => {
    const searchData= ctx.request.body
    let sql = "select * from routes where routeName like ?"
    let route=await ctx.db.EXCUTE(sql, ['%'+searchData.search+'%'])
    let newRouteList = []
    for (let i = (searchData.page - 1) * 5; i < searchData.page * 5; i++) {
        if (route[i]) { newRouteList.push(route[i]) }
    }
    ctx.body = newRouteList
})
//搜索路线总长
router.post("/searchRouteLength", async (ctx, next) => {
    const searchData= ctx.request.body
    let sql = "select * from routes where routeName like ?"
    let route=await ctx.db.EXCUTE(sql, ['%'+searchData.search+'%'])
    ctx.body = route.length
})

module.exports = router