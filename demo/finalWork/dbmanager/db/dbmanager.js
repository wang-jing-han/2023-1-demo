const mysql=require("mysql")

let pools=mysql.createPool({
    host:"localhost",
    user:"root",
    password:"root",
    database:"mydb"
})

function excute(sqlString,condition){
    let promise=new Promise((resolve,reject)=>{
        //sqlString就是sql语句，condition是传的参数，就是问号后面的东西
        pools.query(sqlString,condition,function(error,result,files){
            if(error){
                reject(error)
            }else{
                resolve(result)
            }
        })
    })
    return promise;
}
module.exports={
    EXCUTE:excute
}