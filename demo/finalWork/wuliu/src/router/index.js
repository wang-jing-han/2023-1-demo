import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginView from "../views/LoginView.vue"
import ImageView from "../views/ImageView.vue"
import UserView from "../views/UserView.vue"
import IndexView from "../views/IndexView.vue"
import IndexInnerView from "../views/IndexInnerView.vue"
import RouteView from "../views/RouteView.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: "login",
    component: LoginView
  },
  {
    path: '/',
    name: "index",
    component: IndexView,
    meta:{requireAuth:true},
    children: [
      {
        path: '',
        name: "indexInner",
        component: IndexInnerView
      },
      {
        path: 'user',
        name: "user",
        component: UserView
      },
      {
        path: 'image',
        name: "image",
        component: ImageView
      },
      {
        path: 'route',
        name: "route",
        component: RouteView
      },

    ]
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
