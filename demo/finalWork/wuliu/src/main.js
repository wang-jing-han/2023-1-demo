import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import { instance } from './axios';


Vue.prototype.$axios=instance
Vue.use(ElementUI);
Vue.config.productionTip = false


router.beforeEach((to, from, next) => {
  if (to.meta.requireAuth) {  // 需要权限
      //判断当前是否拥有权限
      if (getUserInfo().user_sub) {
          next();
      } else {  // 无权，跳转登录
        mgr.signinRedirect();
      }
  } else {  // 不需要权限，直接访问
      next();
  }
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
