import axios from "axios"
import {Message} from 'element-ui';

export const instance=axios.create({
  baseURL:"http://localhost:8080/",//是用于请求的服务器 URL
  timeout: 5000, // 请求超时时间 如果请求话费了超过 `timeout` 的时间，请求将被中断
})

//request请求拦截
instance.interceptors.request.use(config => {
  // 添加请求头示例(自行定义)
  config.headers["X-Custom-Header" ]= 'foobar';
  // 必须返回config
  return config
})

// 添加响应拦截器
instance.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  return response;
}, function (error) {
  if(error.response.status==500){
    Message.error("服务端错误")
  }else if(error.response.status==404){
    Message.error("请求地址不存在")
  }
  // 对响应错误做点什么
  return Promise.reject(error);
});
