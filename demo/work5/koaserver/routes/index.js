const router = require('koa-router')()

let user = [
  {
    id:"1",
    username: "王敬邯",
    phone: "15133065628",
    email: "3380524276@qq.com",
    address: "河北省邯郸市肥乡区天台山镇任堡村"
  },
  {
    id:"2",
    username: "李慧超",
    phone: "15227967737",
    email: "2686079948@qq.com",
    address: "河北省邯郸市磁县讲武城镇东曹庄村"
  },
  {
    id:"3",
    username: "zhangsan3",
    phone: "15133065628",
    email: "3380524276@qq.com",
    address: "河北省邯郸市肥乡区天台山镇任堡村"
  },
  {
    id:"4",
    username: "zhangsan4",
    phone: "15133065628",
    email: "3380524276@qq.com",
    address: "河北省邯郸市肥乡区天台山镇任堡村"
  },
  {
    id:"5",
    username: "zhangsan5",
    phone: "15133065628",
    email: "3380524276@qq.com",
    address: "河北省邯郸市肥乡区天台山镇任堡村"
  },
  {
    id:"6",
    username: "zhangsan6",
    phone: "15133065628",
    email: "3380524276@qq.com",
    address: "河北省邯郸市肥乡区天台山镇任堡村"
  },
  {
    id:"7",
    username: "zhangsan7",
    phone: "15133065628",
    email: "3380524276@qq.com",
    address: "河北省邯郸市肥乡区天台山镇任堡村"
  },  {
    id:"8",
    username: "zhangsan8",
    phone: "15133065628",
    email: "3380524276@qq.com",
    address: "河北省邯郸市肥乡区天台山镇任堡村"
  },  {
    id:"9",
    username: "zhangsan9",
    phone: "15133065628",
    email: "3380524276@qq.com",
    address: "河北省邯郸市肥乡区天台山镇任堡村"
  },  {
    id:"10",
    username: "zhangsan10",
    phone: "15133065628",
    email: "3380524276@qq.com",
    address: "河北省邯郸市肥乡区天台山镇任堡村"
  },  {
    id:"11",
    username: "zhangsan11",
    phone: "15133065628",
    email: "3380524276@qq.com",
    address: "河北省邯郸市肥乡区天台山镇任堡村"
  },  {
    id:"12",
    username: "zhangsan12",
    phone: "15133065628",
    email: "3380524276@qq.com",
    address: "河北省邯郸市肥乡区天台山镇任堡村"
  },  {
    id:"13",
    username: "zhangsan13",
    phone: "15133065628",
    email: "3380524276@qq.com",
    address: "河北省邯郸市肥乡区天台山镇任堡村"
  },  {
    id:"14",
    username: "zhangsan14",
    phone: "15133065628",
    email: "3380524276@qq.com",
    address: "河北省邯郸市肥乡区天台山镇任堡村"
  },

]



router.get("/getLength",async(ctx,next)=>{
  ctx.body=user.length
})
router.get("/getUser",async(ctx,next)=>{
  const page=ctx.request.url.split("=")[1]
  let newUserList=[]
  for(let i=(page-1)*5;i<(page*5<user.legth?user.length:page*5);i++){
    if(user[i]){newUserList.push(user[i])}
  }
  ctx.body=newUserList
})
router.post("/changeUser",async(ctx,next)=>{
  for(var i=0;i<user.length;i++){
    if(user[i].id==ctx.request.body.row.id){
      user[i]=ctx.request.body.row
    }
  }
  ctx.body={ "msg": "修改成功", "code": 200 }
})
router.post("/deleteUser",async(ctx,next)=>{
  for(var i=0;i<user.length;i++){
    if(user[i].id==ctx.request.body.row.id){
      user.splice(i,1)
    }
  }
  ctx.body={ "msg": "删除成功", "code": 200 }
})
router.post("/addUser",async(ctx,next)=>{
  user.unshift(ctx.request.body)
  ctx.body={ "msg": "添加成功", "code": 200 }
})
module.exports = router
