const router = require('koa-router')()
const formidable = require('formidable');
let chapterList =[
{
  chapterId:1,
  chapterTitle:"《狂飙》剧组不简单，有人口碑崩坏，有人曾经顶流，有人终于出圈",
  timer:"2023-03-03"
},
{
  chapterId:3,
  chapterTitle:"70岁刘晓庆摘假发直面衰老，穿衣打扮自然得体，满脸皱纹却很舒服",
  timer:"2023-03-03"
},
{
  chapterId:2,
  chapterTitle:"明明有着美人脸，奈何有个汉子名，这9位被名字拖累的女星太可惜",
  timer:"2023-03-03"
}

]

router.get('/', async (ctx, next) => {
  await ctx.render('index', {
    title: 'Hello Koa 2!'
  })
})

router.get('/getData',async(ctx,next)=>{
  console.log('received PC')
  ctx.body = 'received data'
})

router.get('/getList',async(ctx,next)=>{
  ctx.body=chapterList
})
router.post('/editform',async(ctx,next)=>{
  let chapterId = ctx.request.body.chapterId;
  let chapterTitle = ctx.request.body.chapterTitle;
  let timer = ctx.request.body.timer;
  for(let i =0;i<chapterList.length;i++){
    if(chapterList[i].chapterId==chapterId){
      chapterList[i].chapterTitle=chapterTitle;  
      chapterList[i].timer = timer;
      ctx.body = {"msg":"ok","code":200}
      break;
    }
  }
})
router.post("/sub",async(ctx,next)=>{
  console.log(ctx.request.body)
  let chapter={
    chapterId:Math.random(),
    chapterTitle:ctx.request.body.chapterTitle,
    timer:ctx.request.body.timer
  }
  chapterList.push(chapter)
  ctx.body="ok"
})
router.post("/subfrom",async(ctx,next)=>{
  let chapterTitle=ctx.request.body.chapterTitle;
  let timer=ctx.request.body.timer;
  let chapter={
    chapterId:Math.random(),
    chapterTitle:chapterTitle,
    timer:timer
  }
  chapterList.unshift(chapter)
  ctx.body={"code":200,"msg":"success"}
})

router.post("/del",async(ctx,next)=>{
  let id =ctx.request.body.id
  let index=''
  console.log(id)
  for(let i =0;i<chapterList.length;i++){
    if(chapterList[i]["chapterId"]==id){
      index=i
    }
  }
  chapterList.splice(index,1)
  ctx.body="success"
})

router.post("/upload",async(ctx,next)=>{
  console.log(1)
  const form =formidable({
    multiples:true,
    uploadDir:__dirname
  })
  form.parse(ctx.req,async(err,fields,files)=>{
    if(err){
      console.log(err)
    }
    console.log(fields)
    console.log(files)
  })
})


router.get('/string', async (ctx, next) => {
  ctx.body = 'koa2 string'
})

router.get('/json', async (ctx, next) => {
  ctx.body = {
    title: 'koa2 json'
  }
})

module.exports = router
