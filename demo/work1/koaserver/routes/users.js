const router = require('koa-router')()
const svgCaptcha = require('svg-captcha');
let user = require("../data.json")
let currentCaptcha = ""

router.post("/login", async (ctx, next) => {
  if (ctx.request.body.code != currentCaptcha) {
    ctx.body = { "msg": "验证码错误", "code": 430 }
  } else {
    if (!user.some(ele => ele.username === ctx.request.body.username)) {
      ctx.body = { "msg": "该用户不存在", "code": 404 }
    } else {
      for (let i = 0; i < user.length; i++) {
        if (user[i].username == ctx.request.body.username) {
          if (user[i].pwd != ctx.request.body.pwd) {
            ctx.body = { "msg": "密码输入错误", "code": 404 }
          }else{
            ctx.body = { "msg": "登录成功", "code": 200 }
          }
        }
      }
    }
  }
})


router.get('/captcha', async (ctx, next) => {
  const captcha = svgCaptcha.create();
  currentCaptcha = captcha.text
  console.log(currentCaptcha)
  ctx.body = captcha;
});

module.exports = router
