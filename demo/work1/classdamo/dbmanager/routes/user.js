const router = require('koa-router')()


//数据总长度
router.get("/getLength", async (ctx, next) => {
  let sql = "select * from users"
  let user = await ctx.db.EXCUTE(sql)
  ctx.body = user.length
})
//分页下的用户信息
router.get("/getUser", async (ctx, next) => {
  let sql = "select * from users"
  let user = await ctx.db.EXCUTE(sql)
  const page = ctx.request.url.split("=")[1]
  let newUserList = []
  for (let i = (page - 1) * 5; i < (page * 5 < user.legth ? user.length : page * 5); i++) {
    if (user[i]) { newUserList.push(user[i]) }
  }
  ctx.body = newUserList
})
//修改用户信息
router.post("/changeUser", async (ctx, next) => {
  let row=ctx.request.body.row
  let sql = "update users set userName=?,userPhone=?,userEmail=?,userAddress=?,password=? where userId=?"
  let result=await ctx.db.EXCUTE(sql,[row.userName,row.userPhone,row.userEmail,row.userAddress,row.password,row.userId])
  if(result.affectedRows){
    ctx.body = { "msg": "修改成功", "code": 200 } 
  }
})
//删除用户
router.post("/deleteUser", async (ctx, next) => {
  let row=ctx.request.body.row
  let sql="delete from users where userId=?"
  let result=await ctx.db.EXCUTE(sql,[row.userId])
  if(result.affectedRows){
    ctx.body = { "msg": "删除成功", "code": 200 } 
  }
})
//添加用户
router.post("/addUser", async (ctx, next) => {
  let row=ctx.request.body
  let sql="insert into users (userName,userPhone,userEmail,userAddress,password) values (?,?,?,?,?)"
  let result=await ctx.db.EXCUTE(sql,[row.userName,row.userPhone,row.userEmail,row.userAddress,row.password])
  if(result.affectedRows){
    ctx.body = { "msg": "添加成功", "code": 200 } 
  }
})

module.exports = router