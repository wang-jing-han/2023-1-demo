const router = require('koa-router')()
let user = require("../data.json")


//数据总长度
router.get("/getLength", async (ctx, next) => {
  ctx.body = user.length
})
//分页下的用户信息
router.get("/getUser", async (ctx, next) => {
  const page = ctx.request.url.split("=")[1]
  let newUserList = []
  for (let i = (page - 1) * 5; i < (page * 5 < user.legth ? user.length : page * 5); i++) {
    if (user[i]) { newUserList.push(user[i]) }
  }
  ctx.body = newUserList
})
//修改用户信息
router.post("/changeUser", async (ctx, next) => {
  for (var i = 0; i < user.length; i++) {
    if (user[i].id == ctx.request.body.row.id) {
      user[i] = ctx.request.body.row
    }
  }
  ctx.body = { "msg": "修改成功", "code": 200 }
})
//删除用户
router.post("/deleteUser", async (ctx, next) => {
  for (var i = 0; i < user.length; i++) {
    if (user[i].id == ctx.request.body.row.id) {
      user.splice(i, 1)
    }
  }
  ctx.body = { "msg": "删除成功", "code": 200 }
})
//添加用户
router.post("/addUser", async (ctx, next) => {
  user.unshift(ctx.request.body)
  ctx.body = { "msg": "添加成功", "code": 200 }
})

module.exports = router