const router = require('koa-router')()
var svgCaptcha = require('svg-captcha');
let user = require("../data.json")
const jwt = require("jsonwebtoken")
const mykey = "214ii"





//验证码
router.get('/captcha', async (ctx, next) => {
  var captcha = svgCaptcha.create();
  ctx.session.captcha = captcha.text;
  ctx.body = captcha.data;
});
//登录
router.post("/login", async (ctx, next) => {
  if (ctx.request.body.code != ctx.session.captcha) {
    ctx.body = { "msg": "验证码错误", "code": 430 }
  } else {
    if (!user.some(ele => ele.username === ctx.request.body.userName)) {
      ctx.body = { "msg": "该用户不存在", "code": 404 }
    } else {
      for (let i = 0; i < user.length; i++) {
        if (user[i].username == ctx.request.body.userName) {
          if (user[i].pwd != ctx.request.body.pwd) {
            ctx.body = { "msg": "密码输入错误", "code": 404 }
          } else {
            let token = jwt.sign({ userName: ctx.request.body.userName, password: ctx.request.body.pwd }, mykey)
            ctx.body = { token: token, "msg": "登录成功", "code": 200 }
          }
        }
      }
    }
  }
})




module.exports = router
