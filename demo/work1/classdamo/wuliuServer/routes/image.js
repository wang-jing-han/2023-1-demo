const router = require('koa-router')()
const multer = require("koa-multer");
const filelist = []
const path = require('path')
const fs = require("fs")
const mime = require("mime-types")
// 配置
let storage = multer.diskStorage({
    // 文件保存路径, 这里需要自己手动到public下面新建upload文件夹。
    destination: function (req, file, cb) {
      cb(null, "public/images");
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname);
    }
  });
  // 加载配置
  let upload = multer({
    storage: storage,
    limits: {
      fileSize: (1024 * 1024) / 2 // 限制512KB
    }
  });
//上传图片
router.post("/imgUpload", upload.single('file'), async ctx => {
  console.log(ctx.req.file)
    ctx.req.file.key=(new Date()).getTime()
    ctx.response.status = 200;
    if(ctx.req.file.size<500000){
      filelist.push(ctx.req.file)
      // 返回结果给前端
      let data = {
        filename: ctx.req.file.filename
      }
      ctx.body = data
    }else{
      ctx.body={ "msg": "图片过大", "code": 411 }
    }
  
  });
  //删除图片
  router.post("/imgDelete", async(ctx,next) => {
    for(var i=0;i<filelist.length;i++){
      if(filelist[i].key==ctx.request.body.key){
        filelist.splice(i,1)
      }
    }
    ctx.body={ "msg": "删除成功", "code": 200 }
  });
  
  //获取所有图片
  router.get("/allImgs",async(ctx,next)=>{
    ctx.body=filelist
  })
  //获取分页想得到的图片
  router.get("/imgListGet", async (ctx, next) => {
    const page=ctx.request.url.split("=")[1]
    let newfileList=[]
    for(let i=(page-1)*7;i<(page*7<filelist.legth?filelist.length:page*7);i++){
      if(filelist[i]){newfileList.push(filelist[i])}
    }
    ctx.body = newfileList
  })
  //获得具体图片
  router.get("/imgGet", async (ctx, next) => {
    let dirname = __dirname.slice(0, -7)
    let filePath = path.join(dirname, "public/images/" + ctx.url.split("=")[1]); //图片地址
  
    let file = null;
    try {
      file = fs.readFileSync(filePath); //读取文件
    } catch (error) {
      //如果服务器不存在请求的图片，返回默认图片
      filePath = path.join(dirname, 'public/images/default.png'); //默认图片地址
      file = fs.readFileSync(filePath); //读取文件	    
    }
  
    let mimeType = mime.lookup(filePath); //读取图片文件类型
    ctx.set('content-type', mimeType); //设置返回类型
    ctx.body = file; //返回图片
  })

  module.exports = router
  