const router = require('koa-router')()
const fs = require('fs')
const path = require('path'); 

router.get('/', async (ctx, next) => {
  await ctx.render('index', {
    title: 'Hello Koa 2!'
  })
})


router.get("/userlist",async(ctx,next)=>{
  ctx.body=[
    {"userName":"zhangsan","age":20},
    {"userName":"lisi","age":21}
  ]
})

router.get("/imagelist",async(ctx,next)=>{

  let filePath = path.join(__dirname,'/image/1.png'); //图片地址
	let fileContent = fs.readFileSync(filePath); //读取文件
  ctx.type="image/png"
	ctx.body = fileContent; //返回图片
})


router.get('/string', async (ctx, next) => {
  ctx.body = 'koa2 string'
})

router.get('/json', async (ctx, next) => {
  ctx.body = {
    title: 'koa2 json'
  }
})

module.exports = router
